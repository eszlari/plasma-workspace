# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:47+0000\n"
"PO-Revision-Date: 2020-07-06 23:26+0400\n"
"Last-Translator: Xəyyam Qocayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xəyyam Qocayev"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xxmn77@gmail.com"

#: main.cpp:57 view.cpp:46
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:57
#, kde-format
msgid "Run Command interface"
msgstr "Əmr interfeysini işə salmaq"

#: main.cpp:62
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Mübadilə buferi tərkiblərini KRunner üçün sorğu kimi istifadə etmək"

#: main.cpp:63
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "KRunneri arxa planda başlatmaq, onu göstərməmək."

#: main.cpp:64
#, kde-format
msgid "Replace an existing instance"
msgstr "Mövcud nümunə ilə əvəzləmək"

#: main.cpp:69
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "Başlatmaq üçün sorğu, əgər -c açarı göstərilməyibsə istifadə etmək"

#: qml/RunCommand.qml:75
#, kde-format
msgid "Configure"
msgstr ""

#: qml/RunCommand.qml:76
#, kde-format
msgid "Configure KRunner Behavior"
msgstr ""

#: qml/RunCommand.qml:79
#, kde-format
msgid "Configure KRunner…"
msgstr ""

#: qml/RunCommand.qml:91
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr ""

#: qml/RunCommand.qml:92
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr ""

#: qml/RunCommand.qml:228 qml/RunCommand.qml:229 qml/RunCommand.qml:231
#, kde-format
msgid "Show Usage Help"
msgstr ""

#: qml/RunCommand.qml:239
#, kde-format
msgid "Pin"
msgstr ""

#: qml/RunCommand.qml:240
#, kde-format
msgid "Pin Search"
msgstr ""

#: qml/RunCommand.qml:242
#, kde-format
msgid "Keep Open"
msgstr ""

#: qml/RunCommand.qml:320 qml/RunCommand.qml:325
#, kde-format
msgid "Recent Queries"
msgstr ""

#: qml/RunCommand.qml:323
#, kde-format
msgid "Remove"
msgstr ""

#~ msgid "krunner"
#~ msgstr "krunner"
