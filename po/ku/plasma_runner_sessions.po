# translation of krunner_sessions.po to Kurdish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Erdal Ronahi <erdal.ronahi@nospam.gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: krunner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-26 00:48+0000\n"
"PO-Revision-Date: 2008-06-11 14:45+0200\n"
"Last-Translator: Erdal Ronahi <erdal.ronahi@nospam.gmail.com>\n"
"Language-Team: ku <ubuntu-l10n-kur@lists.ubuntu.com>\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KAider 0.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: sessionrunner.cpp:28 sessionrunner.cpp:68
#, kde-format
msgctxt "log out command"
msgid "logout"
msgstr "derketin"

#: sessionrunner.cpp:28
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr ""

#: sessionrunner.cpp:29 sessionrunner.cpp:85
#, fuzzy, kde-format
#| msgctxt "shutdown computer command"
#| msgid "shutdown"
msgctxt "shut down computer command"
msgid "shut down"
msgstr "girtin"

#: sessionrunner.cpp:29
#, fuzzy, kde-format
#| msgid "Shutdown the computer"
msgid "Turns off the computer"
msgstr "Komputerê bigire"

#: sessionrunner.cpp:33 sessionrunner.cpp:94
#, kde-format
msgctxt "lock screen command"
msgid "lock"
msgstr "qufilkirin"

#: sessionrunner.cpp:33
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr ""

#: sessionrunner.cpp:36 sessionrunner.cpp:76
#, kde-format
msgctxt "restart computer command"
msgid "restart"
msgstr "nûdestpêkirin"

#: sessionrunner.cpp:36
#, fuzzy, kde-format
#| msgid "Restart the computer"
msgid "Reboots the computer"
msgstr "Nûdestpêkirina komputerê"

#: sessionrunner.cpp:37 sessionrunner.cpp:77
#, kde-format
msgctxt "restart computer command"
msgid "reboot"
msgstr "nûdestpêkirin"

#: sessionrunner.cpp:40 sessionrunner.cpp:104
#, kde-format
msgctxt "save session command"
msgid "save"
msgstr ""

#: sessionrunner.cpp:40
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:41 sessionrunner.cpp:105
#, fuzzy, kde-format
#| msgid "new session"
msgctxt "save session command"
msgid "save session"
msgstr "danişîna nû"

#: sessionrunner.cpp:44
#, kde-format
msgctxt "switch user command"
msgid "switch"
msgstr "guherandin"

#: sessionrunner.cpp:45
#, fuzzy, kde-format
#| msgctxt "switch user command"
#| msgid "switch"
msgctxt "switch user command"
msgid "switch :q:"
msgstr "guherandin"

#: sessionrunner.cpp:46
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""

#: sessionrunner.cpp:49 sessionrunner.cpp:144
#, kde-format
msgid "switch user"
msgstr "guherandina bikarhênerê/î"

#: sessionrunner.cpp:49
#, kde-format
msgid "Starts a new session as a different user"
msgstr ""

#: sessionrunner.cpp:50 sessionrunner.cpp:144
#, kde-format
msgid "new session"
msgstr "danişîna nû"

#: sessionrunner.cpp:54
#, kde-format
msgid "Lists all sessions"
msgstr ""

#: sessionrunner.cpp:68
#, kde-format
msgid "log out"
msgstr "derkeve"

#: sessionrunner.cpp:70
#, kde-format
msgctxt "log out command"
msgid "Logout"
msgstr "Derketin"

#: sessionrunner.cpp:79
#, kde-format
msgid "Restart the computer"
msgstr "Nûdestpêkirina komputerê"

#: sessionrunner.cpp:86
#, fuzzy, kde-format
#| msgctxt "shutdown computer command"
#| msgid "shutdown"
msgctxt "shut down computer command"
msgid "shutdown"
msgstr "girtin"

#: sessionrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Shutdown the computer"
msgid "Shut down the computer"
msgstr "Komputerê bigire"

#: sessionrunner.cpp:97
#, kde-format
msgid "Lock the screen"
msgstr "Dîmenderê biqufilîne"

#: sessionrunner.cpp:107
#, fuzzy, kde-format
#| msgid "new session"
msgid "Save the session"
msgstr "danişîna nû"

#: sessionrunner.cpp:108
#, kde-format
msgid "Save the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:129
#, fuzzy, kde-format
#| msgid "Sessions"
msgctxt "User sessions"
msgid "sessions"
msgstr "Danişîn"

#: sessionrunner.cpp:150
#, fuzzy, kde-format
#| msgid "switch user"
msgid "Switch User"
msgstr "guherandina bikarhênerê/î"

#: sessionrunner.cpp:227
#, kde-format
msgid "New Session"
msgstr "Danişîna Nû"

#: sessionrunner.cpp:228
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type 'switch' or 'sessions')</li><li>Plasma widgets "
"(such as the application launcher)</li></ul>"
msgstr ""

#~ msgid "Warning - New Session"
#~ msgstr "Hişyarî - Danişîna Nû"

#~ msgid "&Start New Session"
#~ msgstr "&Dest bi danişîna nû bike"
