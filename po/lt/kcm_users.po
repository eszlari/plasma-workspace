# Lithuanian translations for plasma-desktop package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2021-12-12 15:30+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0\n"

#: package/contents/ui/ChangePassword.qml:26
#: package/contents/ui/UserDetailsPage.qml:166
#, kde-format
msgid "Change Password"
msgstr "Keisti slaptažodį"

#: package/contents/ui/ChangePassword.qml:41
#, kde-format
msgid "Password"
msgstr "Slaptažodis"

#: package/contents/ui/ChangePassword.qml:55
#, kde-format
msgid "Confirm password"
msgstr "Pakartokite slaptažodį"

#: package/contents/ui/ChangePassword.qml:68
#: package/contents/ui/CreateUser.qml:65
#, kde-format
msgid "Passwords must match"
msgstr "Slaptažodžiai turi sutapti"

#: package/contents/ui/ChangePassword.qml:73
#, kde-format
msgid "Set Password"
msgstr "Nustatyti slaptažodį"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "Keisti slaptažodinės slaptažodį?"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Pakeitę savo prisijungimo slaptažodį, tikriausiai, norėtumėte pasikeisti ir "
"numatytosios KWallet slaptažodį, kad jie sutaptų."

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "Kas yra KWallet?"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet yra slaptažodžių tvarkytuvė, kurioje saugomi jūsų belaidžių tinklų "
"bei šifruotų išteklių slaptažodžiai. Ji yra užrakinta savo atskiru "
"slaptažodžiu, kuris skiriasi nuo jūsų prisijungimo slaptažodžio. Jei šie du "
"slaptažodžiai sutampa, ji gali būti automatiškai atrakinama prisijungimo "
"metu, kad jums nereikėtų patiems įvedinėti KWallet slaptažodžio."

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "Keisti slaptažodinės slaptažodį"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "Palikti nekeistą"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "Sukurti naudotoją"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Name:"
msgstr "Vardas:"

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "Naudotojo vardas:"

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:149
#, kde-format
msgid "Standard"
msgstr "Standartinis"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Administrator"
msgstr "Administratorius"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:153
#, kde-format
msgid "Account type:"
msgstr "Paskyros tipas:"

#: package/contents/ui/CreateUser.qml:53
#, kde-format
msgid "Password:"
msgstr "Slaptažodis:"

#: package/contents/ui/CreateUser.qml:58
#, kde-format
msgid "Confirm password:"
msgstr "Pakartokite slaptažodį:"

#: package/contents/ui/CreateUser.qml:69
#, kde-format
msgid "Create"
msgstr "Sukurti"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Konfigūruoti piršto atspaudus"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgid "Clear Fingerprints"
msgstr "Išvalyti piršto atspaudus"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Pridėti"

#: package/contents/ui/FingerprintDialog.qml:83
#: package/contents/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Cancel"
msgstr "Atsisakyti"

#: package/contents/ui/FingerprintDialog.qml:89
#, kde-format
msgid "Continue"
msgstr "Tęsti"

#: package/contents/ui/FingerprintDialog.qml:108
#, kde-format
msgid "Done"
msgstr "Atlikta"

#: package/contents/ui/FingerprintDialog.qml:131
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Įrašomas piršto atspaudas"

#: package/contents/ui/FingerprintDialog.qml:137
#, kde-format
msgctxt ""
"%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgstr ""

#: package/contents/ui/FingerprintDialog.qml:147
#, kde-format
msgid "Finger Enrolled"
msgstr "Pirštas įrašytas"

#: package/contents/ui/FingerprintDialog.qml:183
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Pasirinkite pirštą, kurį įrašyti"

#: package/contents/ui/FingerprintDialog.qml:247
#, kde-format
msgid "Re-enroll finger"
msgstr "Įrašyti pirštą iš naujo"

#: package/contents/ui/FingerprintDialog.qml:265
#, kde-format
msgid "No fingerprints added"
msgstr "Nepridėta jokių piršto atspaudų"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "Tvarkyti naudotojus"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "Pridėti naują naudotoją"

#: package/contents/ui/UserDetailsPage.qml:89
#, kde-format
msgid "Choose a picture"
msgstr "Pasirinkti paveikslą"

#: package/contents/ui/UserDetailsPage.qml:120
#, kde-format
msgid "Change avatar"
msgstr "Keisti avatarą"

#: package/contents/ui/UserDetailsPage.qml:162
#, kde-format
msgid "Email address:"
msgstr "El. pašto adresas:"

#: package/contents/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Delete files"
msgstr "Ištrinti failus"

#: package/contents/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Keep files"
msgstr "Palikti failus"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Delete User…"
msgstr "Ištrinti naudotoją…"

#: package/contents/ui/UserDetailsPage.qml:211
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Konfigūruoti tapatybės nustatymą piršto atspaudu…"

#: package/contents/ui/UserDetailsPage.qml:225
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Piršto atspaudai gali būti naudojami vietoje slaptažodžio, atrakinant ekraną "
"ir suteikiant administratoriaus leidimus jų prašančioms įprastoms ar komandų "
"eilutės programoms.<nl/><nl/>Prisijungimas prie sistemos naudojant piršto "
"atspaudą kol kas yra nepalaikomas."

#: package/contents/ui/UserDetailsPage.qml:235
#, kde-format
msgid "Change Avatar"
msgstr "Keisti avatarą"

#: package/contents/ui/UserDetailsPage.qml:238
#, kde-format
msgid "It's Nothing"
msgstr ""

#: package/contents/ui/UserDetailsPage.qml:239
#, kde-format
msgid "Feisty Flamingo"
msgstr "Atkaklus flamingas"

#: package/contents/ui/UserDetailsPage.qml:240
#, kde-format
msgid "Dragon's Fruit"
msgstr "Drakono vaisius"

#: package/contents/ui/UserDetailsPage.qml:241
#, kde-format
msgid "Sweet Potato"
msgstr "Saldi bulvė"

#: package/contents/ui/UserDetailsPage.qml:242
#, kde-format
msgid "Ambient Amber"
msgstr "Supantis gintaras"

#: package/contents/ui/UserDetailsPage.qml:243
#, kde-format
msgid "Sparkle Sunbeam"
msgstr "Tviskantis Saulės spindulys"

#: package/contents/ui/UserDetailsPage.qml:244
#, kde-format
msgid "Lemon-Lime"
msgstr "Citrininė citrina"

#: package/contents/ui/UserDetailsPage.qml:245
#, kde-format
msgid "Verdant Charm"
msgstr "Žaliuojantis žavesys"

#: package/contents/ui/UserDetailsPage.qml:246
#, kde-format
msgid "Mellow Meadow"
msgstr "Sodri pieva"

#: package/contents/ui/UserDetailsPage.qml:247
#, kde-format
msgid "Tepid Teal"
msgstr "Drungna mėlyna"

#: package/contents/ui/UserDetailsPage.qml:248
#, kde-format
msgid "Plasma Blue"
msgstr "Plasma mėlyna"

#: package/contents/ui/UserDetailsPage.qml:249
#, kde-format
msgid "Pon Purple"
msgstr "Pon purpurinė"

#: package/contents/ui/UserDetailsPage.qml:250
#, kde-format
msgid "Bajo Purple"
msgstr "Bajo purpurinė"

#: package/contents/ui/UserDetailsPage.qml:251
#, kde-format
msgid "Burnt Charcoal"
msgstr "Sudegusios anglys"

#: package/contents/ui/UserDetailsPage.qml:252
#, kde-format
msgid "Paper Perfection"
msgstr "Popierinis tobulumas"

#: package/contents/ui/UserDetailsPage.qml:253
#, kde-format
msgid "Cafétera Brown"
msgstr "Kavos aparato ruda"

#: package/contents/ui/UserDetailsPage.qml:254
#, kde-format
msgid "Rich Hardwood"
msgstr "Prabangus kietmedis"

#: package/contents/ui/UserDetailsPage.qml:305
#, kde-format
msgid "Choose File…"
msgstr "Pasirinkti failą…"

#: src/fingerprintmodel.cpp:150 src/fingerprintmodel.cpp:257
#, kde-format
msgid "No fingerprint device found."
msgstr "Nerasta jokio piršto atspaudų įrenginio."

#: src/fingerprintmodel.cpp:316
#, kde-format
msgid "Retry scanning your finger."
msgstr "Iš naujo pabandykite nuskaityti pirštą."

#: src/fingerprintmodel.cpp:318
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Perbraukimas per trumpas. Bandykite dar kartą."

#: src/fingerprintmodel.cpp:320
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Pirštas nėra skaitytuvo viduryje. Bandykite dar kartą."

#: src/fingerprintmodel.cpp:322
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Patraukite pirštą nuo skaitytuvo ir bandykite dar kartą."

#: src/fingerprintmodel.cpp:330
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Nepavyko įrašyti piršto."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Šiam įrenginiui nebeliko vietos. Norėdami tęsti, ištrinkite kitus piršto "
"atspaudus."

#: src/fingerprintmodel.cpp:336
#, kde-format
msgid "The device was disconnected."
msgstr "Įrenginys buvo atjungtas."

#: src/fingerprintmodel.cpp:341
#, kde-format
msgid "An unknown error has occurred."
msgstr "Įvyko nežinoma klaida."

#: src/user.cpp:267
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Nepavyko gauti leidimo įrašyti naudotoją %1"

#: src/user.cpp:272
#, kde-format
msgid "There was an error while saving changes"
msgstr "Įrašant pakeitimus, įvyko klaida"

#: src/user.cpp:368
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr "Nepavyko pakeisti paveikslo dydžio: nepavyko atverti laikinojo failo"

#: src/user.cpp:376
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr "Nepavyko pakeisti paveikslo dydžio: nepavyko rašyti į laikinąjį failą"

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "Jūsų paskyra"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "Kitos paskyros"

#~ msgid "Please repeatedly "
#~ msgstr "Pakartotinai "

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "vardenis.pavardenis@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Moo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "<>"

#~ msgid "Manage user accounts"
#~ msgstr "Tvarkyti naudotojų paskyras"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Vardenis Pavardenis"
