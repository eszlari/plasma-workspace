# Translation of kcm_cursortheme.po to Catalan (Valencian)
# Copyright (C) 2015-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2015, 2016, 2018, 2019, 2020, 2021, 2022.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2022-04-15 16:44+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 21.12.3\n"

#. i18n: ectx: label, entry (cursorTheme), group (Mouse)
#: cursorthemesettings.kcfg:9
#, kde-format
msgid "Name of the current cursor theme"
msgstr "Nom del tema del cursor actual"

#. i18n: ectx: label, entry (cursorSize), group (Mouse)
#: cursorthemesettings.kcfg:13
#, kde-format
msgid "Current cursor size"
msgstr "Mida actual del cursor"

#: kcmcursortheme.cpp:302
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr ""
"Cal tornar a iniciar la sessió de Plasma perquè estos canvis tinguen efecte."

#: kcmcursortheme.cpp:376
#, kde-format
msgid "Unable to create a temporary file."
msgstr "No s'ha pogut crear el fitxer temporal."

#: kcmcursortheme.cpp:387
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr "No s'ha pogut baixar l'arxiu del tema d'icones: %1"

#: kcmcursortheme.cpp:418
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr "El fitxer no és un arxiu vàlid de tema d'icones."

#: kcmcursortheme.cpp:425
#, kde-format
msgid "Failed to create 'icons' folder."
msgstr "No s'ha pogut crear la carpeta «Icones»."

#: kcmcursortheme.cpp:434
#, kde-format
msgid ""
"A theme named %1 already exists in your icon theme folder. Do you want "
"replace it with this one?"
msgstr ""
"A la vostra carpeta de temes d'icones ja existix un tema anomenat %1. Voleu "
"substituir-lo per este?"

#: kcmcursortheme.cpp:438
#, kde-format
msgid "Overwrite Theme?"
msgstr "Sobreescriure el tema?"

#: kcmcursortheme.cpp:462
#, kde-format
msgid "Theme installed successfully."
msgstr "El tema s'ha instal·lat correctament."

#: package/contents/ui/Delegate.qml:54
#, kde-format
msgid "Remove Theme"
msgstr "Elimina el tema"

#: package/contents/ui/Delegate.qml:61
#, kde-format
msgid "Restore Cursor Theme"
msgstr "Restaura el tema del cursor"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you choose the mouse cursor theme."
msgstr "Este mòdul permet configurar el tema del cursor del ratolí."

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Size:"
msgstr "Mida:"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "&Install from File…"
msgstr "I&nstal·la des d'un fitxer…"

#: package/contents/ui/main.qml:140
#, kde-format
msgid "&Get New Cursors…"
msgstr "O&btín cursors nous…"

#: package/contents/ui/main.qml:157
#, kde-format
msgid "Open Theme"
msgstr "Obri un tema"

#: package/contents/ui/main.qml:159
#, kde-format
msgid "Cursor Theme Files (*.tar.gz *.tar.bz2)"
msgstr "Fitxers de tema del cursor (*.tar.gz *.tar.bz2)"

#: plasma-apply-cursortheme.cpp:42
#, kde-format
msgid "The requested size '%1' is not available, using %2 instead."
msgstr ""
"La mida sol·licitada «%1» no està disponible, en el seu lloc s'utilitza %2."

#: plasma-apply-cursortheme.cpp:63
#, kde-format
msgid ""
"This tool allows you to set the mouse cursor theme for the current Plasma "
"session, without accidentally setting it to one that is either not "
"available, or which is already set."
msgstr ""
"Esta eina permet definir el tema del cursor de ratolí per a la sessió actual "
"de Plasma, sense posar-ne accidentalment un que no estiga disponible, o que "
"ja estiga definit."

#: plasma-apply-cursortheme.cpp:67
#, kde-format
msgid ""
"The name of the cursor theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""
"El nom del tema del cursor que voleu definir a la sessió actual de Plasma "
"(en passar un camí complet només s'utilitzarà l'última part del camí)"

#: plasma-apply-cursortheme.cpp:68
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr ""
"Mostra tots els temes disponibles en el sistema (i quin és el tema actual)"

#: plasma-apply-cursortheme.cpp:69
#, kde-format
msgid "Use a specific size, rather than the theme default size"
msgstr ""
"Utilitza una mida específica, en lloc de la mida predeterminada del tema"

#: plasma-apply-cursortheme.cpp:90
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""
"El tema sol·licitat «%1» ja està establit com a tema de la sessió actual de "
"Plasma."

#: plasma-apply-cursortheme.cpp:101
#, kde-format
msgid ""
"Successfully applied the mouse cursor theme %1 to your current Plasma session"
msgstr ""
"El tema del cursor de ratolí %1 s'ha aplicat correctament a la sessió actual "
"de Plasma"

#: plasma-apply-cursortheme.cpp:103
#, kde-format
msgid ""
"You have to restart the Plasma session for your newly applied mouse cursor "
"theme to display correctly."
msgstr ""
"Cal tornar a iniciar la sessió de Plasma perquè s'aplique correctament el "
"tema nou de cursor de ratolí."

#: plasma-apply-cursortheme.cpp:113
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""
"No s'ha pogut trobar el tema «%1». El tema hauria de ser una de les opcions "
"següents: %2"

#: plasma-apply-cursortheme.cpp:121
#, kde-format
msgid "You have the following mouse cursor themes on your system:"
msgstr "En el sistema hi ha els temes següents de cursor de ratolí:"

#: plasma-apply-cursortheme.cpp:126
#, kde-format
msgid "(Current theme for this Plasma session)"
msgstr "(Tema actual d'esta sessió de Plasma)"

#: xcursor/xcursortheme.cpp:60
#, kde-format
msgctxt ""
"@info The argument is the list of available sizes (in pixel). Example: "
"'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
msgid "(Available sizes: %1)"
msgstr "(Mides disponibles: %1)"
